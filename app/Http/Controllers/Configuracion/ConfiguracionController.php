<?php

namespace App\Http\Controllers\Configuracion;

use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Facades\JWTAuth;

class ConfiguracionController extends Controller
{
    //
    protected function responder($mensaje = null, $data, $codigoHttp = 200, $paginado = false)
    {
        $response = array();
        $response['codigo'] = $codigoHttp;
        $response['mensaje'] = $mensaje;
        $token = JWTAuth::getToken();

        if ($data) {
            /* Verificamos si requiere de resultados paginados */
            if ($paginado) {
                $data = $this->paginar($data);
            }
            $response['data'] = $data;

        } else {
            $response['data'] = $data;
        }

        return response()->json($response, $codigoHttp, self::getHeaders($token), JSON_UNESCAPED_UNICODE);
    }

    protected function responderError($data, $error = 'G0009', $codigoHttp = 400, $arrayError = null)
    {
        $err = $this->VC_Error($error, $arrayError);
        return response()->json(['codigo' => $codigoHttp, 'error' => $err, 'data' => $data], $codigoHttp, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Metodo para mostrar en el JSON de respuesta informacion de los errores
     * a traves del archivo de gestion de errores
     * TODO:
     * Este metodo aun se encuentra en desarrollo.
     *
     * @param [type] $cod
     * @param [type] $extra
     * @param bool   $showExtra
     */
    private static function VC_Error($cod, $extra = null, $showExtra = true)
    {
        $isLocal = (env('APP_ENV') == 'local') ? true : false;

        $errCode = [
            'codigo' => 'undefined',
            'mensaje' => 'Acceso denegado',
            'info' => ($showExtra) ? $extra : ($isLocal) ? $extra : null,
        ];

        if (is_array($cod)) {
            return $cod;
        }

        $errValue = config('errores.lista.errores.'.$cod);

        if ($errValue == null) {
            return $errCode;
        }

        $errCode['codigo'] = $cod;

        $public = null;
        $private = null;

        $explode = explode('|', $errValue);

        foreach ($explode as $v) {
            $mensaje = '';
            if (str_contains($v, 'public:')) {
                /*
                 * Verificamos si existe el objeto y posee un public
                 */
                if ($extra != null && array_key_exists('public', $extra)) {
                    $mensaje = $extra['public'];
                }
                $v = str_replace('{{mensaje}}', $mensaje, $v);
                $public = trim(str_replace('public:', '', $v));
            } elseif (str_contains($v, 'private:')) {
                /*
                 * Reemplazamos en el private el elemento comodin del programador.
                 */
                if ($extra != null && array_key_exists('private', $extra)) {
                    $mensaje = $extra['private'];
                }
                $v = str_replace('{{mensaje}}', $mensaje, $v);
                $private = trim(str_replace('private:', '', $v));
            }
        }

        if (!$public) {
            $public = 'Acceso denegado';
        }

        if (!$private) {
            $private = $public;
        }

        $errCode['mensaje'] = ($isLocal) ? $private : $public;

        return $errCode;
    }


    /**
     * Metodo privado para setear la informacion de la cabeceras de respuesta.
     *
     * @param [type] $token
     */
    private static function getHeaders($token)
    {
        $headers = array();
        $headers['Access-Control-Allow-Methods'] = 'POST, GET, OPTIONS, PUT, DELETE';
        $headers['Access-Control-Allow-Headers'] = 'Content-Type, X-Auth-Token, Origin, Authorization, User-Agent, app-access, X-Requested-With';
        $headers['Content-Type'] = 'application/json';
        $headers['Charset'] = 'UTF-8';
        $headers['Access-Control-Expose-Headers'] = 'X-Auth-Token';
        $headers['X-Auth-Token'] = $token;

        return $headers;
    }

}
