<?php

namespace App\Http\Controllers\Producto;

use App\Http\Controllers\Configuracion\ConfiguracionController;
use App\Models\Producto\Producto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Tymon\JWTAuth\Facades\JWTAuth;
use Intervention\Image\Facades\Image;

class ProductoController extends ConfiguracionController
{
    //
    public function index()
    {
        //
        try {
            $datos = Producto::all();
            $this->cargarBelognsTo($datos);
            return $this->responder('OK', $datos, 200);
        }catch (\Exception $e){
            return $this->responderError(null, 'G3004', 400, $e);
        }
    }

    public function store(Request $request)
    {
        //
        try {
            DB::beginTransaction();
            $user = JWTAuth::parseToken()->authenticate();
            $request->merge(['creado_por' => $user->id]);
            $guardar = new Producto();
            $guardar->fill($request->all());
            $guardar->save();
            $this->imagen($request, $guardar->id);
            $datos = Producto::all();
            $this->cargarBelognsTo($datos);
            DB::commit();
            return $this->responder('OK', $datos, 200);
        }catch (\Exception $e){
            DB::rollBack();
            return $this->responderError(null, 'G3001', 400, $e);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        //
        try {
            DB::beginTransaction();
            $user = JWTAuth::parseToken()->authenticate();
            $request->merge(['actualizado_por' => $user->id]);
            $actualizar = Producto::find($id);
            $actualizar->fill($request->all());
            if($actualizar->update()){
                $this->imagen($request, $actualizar->id);
            }
            $datos = Producto::all();
            $this->cargarBelognsTo($datos);
            DB::commit();
            return $this->responder('OK', $datos, 200);
        }catch (\Exception $e){
            DB::rollBack();
            return $this->responderError(null, 'G3002', 400, $e);
        }
    }


    public function destroy($id)
    {
        //
        try {
            DB::beginTransaction();
            $eliminar = Producto::find($id);
            $eliminar->delete();
            $datos = Producto::all();
            $this->cargarBelognsTo($datos);
            DB::commit();
            return $this->responder('OK', $datos, 200);
        }catch (\Exception $e){
            DB::rollBack();
            return $this->responderError(null, 'G3003', 400, $e);
        }
    }

    public function imagen($request, $id){

        $producto = Producto::find($id);
        if ($request->hasFile('ruta_imagen')) {
            $image = $request->file('ruta_imagen');
            $ruta = 'productos/producto_'.$producto->id.'.png';
            $img = Image::make($image->getRealPath());
            $img->stream();
            Storage::disk('public')->put($ruta, $img, 'public');
            $producto->ruta_imagen = $ruta;
            $producto->update();
        }
    }

    public function show($id){
        try {
            $datos = Producto::find($id);
            $this->cargarBelongTo($datos);
            return $this->responder('OK', $datos, 200);
        }catch (\Exception $e){
            return $this->responderError(null, 'G3004', 400, $e);
        }
    }

    public function cargarBelognsTo($datos){
        foreach ($datos as $dato){
           $this->cargarBelongTo($dato);
        }
    }

    public function cargarBelongTo($dato){
        $dato->creado;
        $dato->actualizado;
        $dato->ruta_producto = $dato->ruta();
    }
}
