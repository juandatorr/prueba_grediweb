<?php

namespace App\Http\Controllers\Usuario;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class AutenticacionController extends Controller
{
    //
    public function authenticate(Request $request) {

        $user = User::where([
            ['email', $request->email],
            ['password', $request->password]
        ])->first();

        try {
            if ($user) {
                if (!$token = JWTAuth::fromUser($user)) {
                    return response()->json(['error' => 'Datos invalidos'], 401);
                }
            } else {
                return response()->json(['error' => 'Los datos ingresados no son validos'], 402);
            }

        } catch (JWTException $e) {
            return response()->json(['error' => 'No se pudo crear el token ' . $e], 500);
        }


        $response = compact('token');
        $response['usuario'] = $user;

        return $response;
    }
}
