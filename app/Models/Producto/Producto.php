<?php

namespace App\Models\Producto;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Producto extends Model
{
    //
    use SoftDeletes;
    protected $table = 'producto';
    protected $primaryKey = 'id';

    protected $fillable = ['nombre', 'descripcion', 'ruta_imagen', 'precio', 'creado_por', 'actualizado_por'];

    public function creado(){
        return $this->belongsTo('App\User', 'creado_por', 'id');
    }

    public function actualizado(){
        return $this->belongsTo('App\User', 'actualizado_por', 'id');
    }

    public function ruta(){
        return asset('storage/'."{$this->ruta_imagen}");
    }
}
