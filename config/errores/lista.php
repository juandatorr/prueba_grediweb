<?php
/**
 * Archivo de control y gestión de errores, cada modulo tiene la postestad de crear sus propios codigos
 * de error, pero deben seguir con el lineamiento, primero buscar el error dentro de "ERRORES GENERALES"
 * Si no lo encuentran pueden crearlo de la siguiente forma.
 *
 * LETRASXXXX : donde la LETRAS son las iniciales del modulo y XXXX el numero del error por ejemplo:
 * 'ED0001' => 'public:Descripcion publica|private:descripcion privada'
 * es el error 0001 del modulo de EDUCACION
 *
 * public: es la descripcion que se mostrara a los usuarios.
 *
 * private: es el codigo que se le mostrara al programador, por favor en esta descripcion ser especifico
 *          por que el encargado de soporte tendra acceso a este error mediante el sistema del modulo
 *          de gestion de errores (aun en desarrollo).
 *
 * NOTA: Si eno hay mensaje para el usuario pero si para el programador pueden iniciar con private: XXXXXX
 * donde XXXX es el mensaje para el programador o gestor
 */

return [
    'errores' => [
        /* ERRORES GENERALES G0XXX*/
        /* ERRORES GENERALES DE AUTENTICACION G1XXX*/
        /* ERRORES GENERALES DE TOKEN  G2XXX */
        /* ERROR DE BASE DE DATOS G3XXX*/
        'G3000' => 'private:*************** Códigos de error en insercion de datos en las tablas ***************',
        'G3001' => 'public:Error al insertar el registro|private:Error en el metodo store del controlador',
        'G3002' => 'public:Error al actualizar el registro|private:Error en el metodo update del controlador',
        'G3003' => 'public:Error al eliminar el registro|private:Error en el metodo destroy del controlador',
        'G3004' => 'public:Error al realizar la consulta|private:Error en el SQL de la consulta'
    ],
];
