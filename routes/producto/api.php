<?php

Route::group(['prefix' => 'producto', 'middleware' => ['jwt.auth']], function(){
    Route::resource('producto', 'Producto\ProductoController');
});
